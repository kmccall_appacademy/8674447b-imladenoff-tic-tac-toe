class Board
  attr_reader :grid, :left_di, :right_di

  def initialize grid=3
    @grid = Array.new(grid) { Array.new(grid) }
    @left_di = diagonals(:l)
    @right_di = diagonals(:r)
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    grid[row][col] = mark
  end

  def place_mark pos, mark
    raise "That space is already taken." if !empty?(pos)
    self[pos] = mark
  end

  def empty? pos
    self[pos] == nil
  end

  def winner #update to take grid of larger sizes
    grid.each do |row|
      return :X if row == [:X,:X,:X]
      return :O if row == [:O,:O,:O]
    end
    cols = grid.transpose
    cols.each do |col|
        return :X if col == [:X,:X,:X]
        return :O if col == [:O,:O,:O]
    end
    return :O if left_di.map { |pos| self[pos] } == [:O,:O,:O]
    return :X if left_di.map { |pos| self[pos] } == [:X,:X,:X]
    return :O if right_di.map { |pos| self[pos] } == [:O,:O,:O]
    return :X if right_di.map { |pos| self[pos] } == [:X,:X,:X]
    nil
  end

  def diagonals dir
    max_i = grid.count - 1
    if dir == :l
      return (0..max_i).map { |num| [num,num] }
    elsif dir == :r
      return (0..max_i).map { |num| [num,(max_i - num)] }
    end
  end

  def over?
    return true if !grid.flatten.include?(nil)
    return true if winner
  end

end
