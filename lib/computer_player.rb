class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize name
    @name = name
  end

  def get_move
    #refactor methods
    #block winning moves
    #win/block on larger grids
    puts "Computer moves: (press enter)"
    gets
    moves = []
    board.grid.each.with_index do |row,i|
      row.each.with_index do |cell,j|
          moves << [i,j] if board[[i,j]] == nil
      end
    end
    moves.each do |move|
      board[move] = mark
      if board.winner == mark
        board[move] = nil
        return move
      else
        board[move] = nil
      end
    end

    moves.sample
  end

  def display board
    @board = board
  end
end
