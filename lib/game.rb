require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :current_player
  attr_accessor :board

  def initialize player1, player2
    @player1, @player2 = player1, player2
    @board = Board.new
    @player1.mark = :X
    @player2.mark = :O
    @current_player = @player1
  end

  def switch_players!
    @current_player == @player1 ? @current_player = @player2 : @current_player = @player1
  end

  def play_turn
    @current_player.display board
    board.place_mark @current_player.get_move, @current_player.mark
    @current_player.display board
    switch_players!
  end

  def play
    puts "#{@player1.name} you are #{@player1.mark}s. You go first. (press 'enter')"
    gets
    while !board.over?
      play_turn
    end
    puts board.winner != nil ? "#{board.winner}s won!" : "Cat's game, me-ow!"
  end

end

if $PROGRAM_NAME == __FILE__

  print "Who is playing? "
  name = gets.chomp
  human = HumanPlayer.new(name)
  computer = ComputerPlayer.new('computer')

  game = Game.new(human, computer)
  game.play
end
