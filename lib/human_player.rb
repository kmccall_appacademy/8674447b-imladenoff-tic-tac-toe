class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize name
    @name = name
  end

  def get_move
    print "Where would you like to move (row,column)? "
    move = gets.chomp
    pos = [move[0].to_i,move[-1].to_i]
  end

  def display board #should be updated to take a grid of any size
    puts "\n"
    puts "       columns"
    puts "        0 1 2"
    puts "       ┌─#{"┬─" * (board.grid.count - 1)}┐"
    puts "     0 │#{render_row board.grid[0]}│"
    puts "       ├─┼─┼─┤"
    puts "rows 1 │#{render_row board.grid[1]}│"
    puts "       ├─┼─┼─┤"
    puts "     2 │#{render_row board.grid[2]}│"
    puts "       └─#{"┴─" * (board.grid.count - 1)}┘\n\n"
  end

  def render_row row
    (row.map { |cell| cell == nil ? " " : cell }).join "│"
  end
end
